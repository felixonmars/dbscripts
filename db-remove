#!/bin/bash

. "$(dirname "$(readlink -e "$0")")/config"
. "$(dirname "$(readlink -e "$0")")/db-functions"

if (( $# < 3 )); then
	msg "usage: %s <repo> <arch> <pkgname|pkgbase> ..." "${0##*/}"
	exit 1
fi

repo="$1"
arch="$2"
pkgbases=("${@:3}")

vcsrepo="$repo-$arch"

if ! check_repo_permission "$repo"; then
	die "You don't have permission to remove packages from %s" "$repo"
fi
if ! check_author; then
	die "You don't have a matching author mapping"
fi

if [[ $arch = any ]]; then
	tarches=("${ARCHES[@]}")
else
	tarches=("$arch")
fi

for tarch in "${tarches[@]}"; do
	repo_lock "$repo" "$tarch" || exit 1
done

remove_pkgbases=()
remove_pkgs=()
remove_debug_pkgs=()
for pkgbase in "${pkgbases[@]}"; do
	msg "Removing %s from [%s]..." "$pkgbase" "$repo"

	if fetch_pkgbuild "${pkgbase}" && \
			_pkgver=$(pkgver_from_state_repo "${pkgbase}" "${vcsrepo}") && \
			remove_pkgs+=($(source_pkgbuild "${pkgbase}" "${_pkgver}" && printf "%s\n" "${pkgname[@]}")); then
		remove_pkgbases+=("${pkgbase}")
	else
		warning "pkgbase %s not found in %s" "$pkgbase" "$vcsrepo"
		warning "Removing only pkgname %s from the repo" "$pkgbase"
		warning "If it was a split package you have to pass its pkgbase to remove it completely!"
		remove_pkgs+=("$pkgbase")
	fi
	if is_globfile "${FTP_BASE}/${repo}-debug/os/${tarch}/${pkgbase}-debug"*; then
		msg "Found debug package. Removing %s from [%s]..." "${pkgbase}-debug" "${repo}-debug"
		remove_debug_pkgs+=("${pkgbase}-debug")
	fi
done

for tarch in "${tarches[@]}"; do
	if (( ${#remove_pkgs[@]} >= 1 )); then
		arch_repo_modify remove "${repo}" "${tarch}" "${remove_pkgs[@]}"
	fi
	if (( ${#remove_debug_pkgs[@]} >= 1 )); then
		arch_repo_modify remove "${repo}-debug" "${tarch}" "${remove_debug_pkgs[@]}"
	fi
done

if ((REPO_MODIFIED)); then
	for pkgbase in "${remove_pkgbases[@]}"; do
		vcs_remove_package "${pkgbase}" "${vcsrepo}"
	done
fi

# Remove all the locks we created
for tarch in "${tarches[@]}"; do
	repo_unlock "$repo" "$tarch"
done
